//  Sentence.java

//  < SENT >  ::= < SNP > < SVP >  | < PNP > < PVP >
//  < SNP >   ::= < ART > < NOUN > | < ART > < ADJ > < NOUN >
//  < PNP >   ::= < SNP > and < NP >
//  < NP >    ::= < PNP > | < SNP >
//  < SVP >   ::= < SVERB > | < SVERB > < ADV > | < SVERB > < PP > | < SVERB > < ADV > < PP >
//  < PVP >   ::= < PVERB > | < PVERB > < ADV > | < PVERB > < PP > | < PVERB > < ADV > < PP >
//  < PP >    ::= < PREP > < NP > < PP > | < PREP > < NP >
//  < NOUN >  ::= boy | girl | man | woman | dog | cat | tree | car | ball 
//  < SVERB > ::= runs | walks | drives | calls | talks | climbs | falls
//  < PVERB > ::= run | walk | drive | call | talk | climb | fall
//  < PREP >  ::= to | from | by | up | after | before | beside | into
//  < ADV >   ::= quickly | slowly | boldly | fearfully | tearfully | rapidly | confidently
//  < ADJ >   ::= big | small | short | tall | red | blue | strong | weak
//  < ART >   ::= the | a

//<EXP> ::= <MOD> { +|- <MOD> }
//<MOD> ::= <PROD> { % <PROD> }
//<PROD> ::= <POWER> { *|/ <POWER> }
//<POWER> ::= <THING> ^ <THING>
//<THING> ::= (<EXP) | <NUMBER>
//<NUMBER> ::= <DIGIT> { <DIGIT> }
//<DIGIT>  ::= 0|1|2|3|4|5|6|7|8|9


import java.io.*;
import java.util.StringTokenizer;

class Parse {
    protected static final int MAX_TIMES = 100;

    protected static String[] tokenList;
    protected static int currToken;

    protected static boolean lookup(String word, String[] list) {
	for ( int i = 0;  i < list.length;  i++ ) {
	    if ( word.equals(list[i]) ) {
		return true;  //  Found it
	    }
	}
	return false;  //  Not there
    }

    protected static java.util.Random random = new java.util.Random();

    protected static int nextInt(int n) {
	return Math.abs(random.nextInt()) % n;
    }

    //  Literal words
    protected static String[] nouns = { "boy", "girl", "man", "woman", "dog", "cat", "tree", "car", "ball" };
    protected static String[] singleverbs = { "runs", "walks", "drives", "calls", "talks", "climbs", "falls" };
    protected static String[] pluralverbs = { "run", "walk", "drive", "call", "talk", "climb", "fall" };
    protected static String[] prepositions = { "to", "from", "by", "up", "after", "before", "beside", "into" };
    protected static String[] adverbs = { "quickly", "slowly", "boldly", "fearfully", "tearfully", "rapidly", "confidently"};
    protected static String[] adjectives = { "big", "small", "short", "tall", "red", "blue", "strong", "weak" };
    protected static String[] articles = { "the", "a" };

/*
    //  Generate a random word
    protected static String genNoun() {
	return nouns[nextInt(nouns.length)];
    }

    protected static String genPluralVerb() {
	return pluralverbs[nextInt(pluralverbs.length)];
    }

    protected static String genSingleVerb() {
	return singleverbs[nextInt(singleverbs.length)];
    }

    protected static String genPreposition() {
	return prepositions[nextInt(prepositions.length)];
    }

    protected static String genAdjective() {
	return adjectives[nextInt(adjectives.length)];
    }

    protected static String genAdverb() {
	return adverbs[nextInt(adverbs.length)];
    }

    protected static String genArticle() {
	return articles[nextInt(articles.length)];
    }

    //    < SENT >  ::= < SNP > < SVP >  | < PNP > < PVP >
    protected static String genSentence() {
	if ( nextInt(2) == 0 ) {
		return genSingleNounPhrase() + " " + genSingleVerbPhrase();
	}
	else {
		return genPluralNounPhrase() + " " + genPluralVerbPhrase();
	}	
    }

    //  < SNP >   ::= < ART > < NOUN > | < ART > < ADJ > < NOUN >
    protected static String genSingleNounPhrase() {
	String result = genArticle();
	if ( nextInt(2) == 0 ) {
		result += " " + genAdjective();
	}
		result += " " + genNoun();
	return result;
    }

    //  < PNP >   ::= < SNP > and < NP >
    protected static String genPluralNounPhrase() {
	
	return genSingleNounPhrase() + " and " + genNounPhrase();

    }  

    protected static String genNounPhrase() {

	if ( nextInt(2) == 0 ) {
		return genPluralNounPhrase();
	}
	else {
		return genSingleNounPhrase();
	}
    }

    //  < SVP >   ::= < SVERB > | < SVERB > < ADV > | < SVERB > < PP > | < SVERB > < ADV > < PP >
    protected static String genSingleVerbPhrase() {
	String result = genSingleVerb();
	if ( nextInt(2) == 0) {
		result += " " + genAdverb();
	}
	if ( nextInt(2) == 0) {
		result += " " + genPrepositionPhrase();
	}
	return result;
    }


    //  < PVP >   ::= < PVERB > | < PVERB > < ADV > | < PVERB > < PP > | < PVERB > < ADV > < PP >
    protected static String genPluralVerbPhrase() {
	String result = genPluralVerb();
	if ( nextInt(2) == 0) {
		result += " " + genAdverb();
	}
	if ( nextInt(2) == 0) {
		result += " " + genPrepositionPhrase();
	}
	return result;
    }


    //  < PP >    ::= < PREP > < NP > < PP > | < PREP > < NP >
    protected static String genPrepositionPhrase() {
	String result = genPreposition() + " " + genNounPhrase();
	if ( nextInt(2) == 0) {
		result += " " + genPrepositionPhrase();
	}
	return result;
    } 
*/










    //  < SENT >  ::= < SNP > < SVP >  | < PNP > < PVP >
    protected static boolean parseSentence() {
   	int savePosition = currToken;
    	boolean result = parseSingleNounPhrase() && parseSingleVerbPhrase();
    	if (!(result)) {
		currToken = savePosition;
		// System.out.println("This was a SENT");
    		return parsePluralNounPhrase() && parsePluralVerbPhrase() && currToken == tokenList.length;
    	}
    	return result && currToken == tokenList.length;
    }


    //  < SNP >   ::= < ART > < NOUN > | < ART > < ADJ > < NOUN >
    
    protected static boolean parseSingleNounPhrase() {
    	int savePosition = currToken;
    	boolean result = parseArticle();
   	if (result) {
		parseAdjective();
		// System.out.println("This was a SNP");
		return parseNoun();
	}
	currToken = savePosition;
	return false;
    }

    //  < PNP >   ::= < SNP > and < NP >
    protected static boolean parsePluralNounPhrase() {
   	int savePosition = currToken; 
	boolean result = parseSingleNounPhrase();
		if (result) {
			currToken++;
			// System.out.println("This is a PNP");
			return parseNounPhrase();
		}
	currToken = savePosition;
	return false;	
    }
    
    //  < NP >    ::= < PNP > | < SNP >
    protected static boolean parseNounPhrase() {
	int savePosition = currToken;
	boolean result = parsePluralNounPhrase();
    	if (!(result)) {
		currToken = savePosition;
		// System.out.println("This is a SNP");
		return parseSingleNounPhrase();
	}
	// System.out.println("This is a PNP");
	return result;
    }

    //  < SVP >   ::= < SVERB > | < SVERB > < ADV > | < SVERB > < PP > | < SVERB > < ADV > < PP >
    protected static boolean parseSingleVerbPhrase() {
    	int savePosition = currToken;
    	boolean res1 = parseSingleVerb();
		if (res1) {
			parseAdverb();
			parsePrepPhrase();
			// System.out.println("This was a SVP");
			return true;
		}
	currToken = savePosition;
	return false;
    }
    
    //  < PVP >   ::= < PVERB > | < PVERB > < ADV > | < PVERB > < PP > | < PVERB > < ADV > < PP >
    protected static boolean parsePluralVerbPhrase() {
    	int savePosition = currToken;
    	boolean res1 = parsePluralVerb();
		if (res1) {
			parseAdverb();
			parsePrepPhrase();
			// System.out.println("This was a PVP");
			return true;
		}
	currToken = savePosition;
	return false;
    }

    //  < PP >    ::= < PREP > < NP > < PP > | < PREP > < NP >
    
    protected static boolean parsePrepPhrase() {
	    int savePosition = currToken;
	    if ( parsePrep() ) {
		if ( parseNounPhrase() ) {
		    savePosition = currToken;
		    if ( !parsePrepPhrase() ) {
			//  Not another PP
			currToken = savePosition; //  Restore previous position
		    }
		    // System.out.println("This was a PP");
		    return true;
		} 
	    }
	    currToken = savePosition; //  Restore previous position
	    return false;
    }








//
//
//
//  Starting word stuff.
//
//
//



    //  < NOUN >  ::= boy | girl | man | woman | dog | cat | tree | car | ball 

    protected static boolean parseNoun() {
	if ( currToken < tokenList.length ) {
	    boolean result = lookup(tokenList[currToken], nouns);
	    if ( result ) {
		// System.out.print(tokenList[currToken] + " [noun] ");
		currToken++;
		return true;
	    }
	}
	return false;
    }

//  < SVERB > ::= runs | walks | drives | calls | talks | climbs | falls

    protected static boolean parseSingleVerb() {
	if ( currToken < tokenList.length ) {
	    boolean result = lookup(tokenList[currToken], singleverbs);
	    if ( result ) {
		// System.out.print(tokenList[currToken] + " [singleverb] ");
		currToken++;
		return true;
	    }
	}
	return false;
    }

//  < PVERB > ::= run | walk | drive | call | talk | climb | fall

    protected static boolean parsePluralVerb() {
	if ( currToken < tokenList.length ) {
	    boolean result = lookup(tokenList[currToken], pluralverbs);
	    if ( result ) {
		// System.out.print(tokenList[currToken] + " [pluralverb] ");
		currToken++;
		return true;
	    }
	}
	return false;
    }

   //  < PREP >  ::= to | from | by | up | after | before | beside | into
   protected static boolean parsePrep() {
	if ( currToken < tokenList.length ) {
	    boolean result = lookup(tokenList[currToken], prepositions);
	    if ( result ) {
                // System.out.print(tokenList[currToken] + " [preposition] ");
		currToken++;
		return true;
	    }
	}
	return false;
    }

    //  < ADV >   ::= quickly | slowly | boldly | fearfully | tearfully | rapidly | confidently
    protected static boolean parseAdverb() {
	if ( currToken < tokenList.length ) {
	    boolean result = lookup(tokenList[currToken], adverbs);
	    if ( result ) {
                // System.out.print(tokenList[currToken] + " [adverb] ");
		currToken++;
		return true;
	    }
	}
	return false;
    }

    //  < ADJ >   ::= big | small | short | tall | red | blue | strong | weak
    protected static boolean parseAdjective() {
	if ( currToken < tokenList.length ) {
	    boolean result = lookup(tokenList[currToken], adjectives);
	    if ( result ) {
                // System.out.print(tokenList[currToken] + " [adjective] ");
		currToken++;
		return true;
	    }
	}
	return false;
    }

    //  < ART >   ::= the | a
    protected static boolean parseArticle() {
	if ( currToken < tokenList.length ) {
	    boolean result = lookup(tokenList[currToken], articles);
	    if ( result ) {
                // System.out.print(tokenList[currToken] + " [article] ");
		currToken++;
		return true;
	    }
	}
	return false;
    }






























/*
    
    protected static boolean parseSentence() {
	//  Noun phrase followed by verb phrase and the string
	//  has nothing else following
	return parseNounPhrase() && parseVerbPhrase() 
	       && currToken == tokenList.length;
    }

    //  <NP>   ::= <ART> <NOUN> | <ART> <ADJ> <NOUN>
    protected static boolean parseNounPhrase() {
	boolean result = parseArticle();
	if ( result ) {
	    parseAdjective();
            // System.out.println("NP ok");
	    return parseNoun();
	}
	return false;  //  Failure
    }

    //  <VP>   ::= <VERB> | <VERB> <ADV> | <VERB> <PP> | <VERB> <ADV> <PP>
    protected static boolean parseVerbPhrase() {
	if ( currToken < tokenList.length ) {
	    //  Remember where we started
	    int savePosition = currToken;
	    boolean result = parseVerb();
	    if ( result ) {
		parseAdverb();
		parsePrepPhrase();
		return true;
	    }
	    currToken = savePosition; //  Restore previous position
	}
	return false;  //  Failure
    }

    //  <PP>   ::= <PREP> <NP> <PP> | <PREP> <NP>
    protected static boolean parsePrepPhrase() {
	if ( currToken < tokenList.length ) {
	    //  Remember where we started
	    int savePosition = currToken;
	    if ( parsePreposition() ) {
		if ( parseNounPhrase() ) {
		    savePosition = currToken;
		    if ( !parsePrepPhrase() ) {
			//  Not another PP
			currToken = savePosition; //  Restore previous position
		    }
		    return true;
		} 
	    }
	    currToken = savePosition; //  Restore previous position
	}
	return false;
    }

    //  <NOUN> ::= boy | girl | man | woman | dog | cat | tree | car | ball 
    protected static boolean parseNoun() {
	if ( currToken < tokenList.length ) {
	    boolean result = lookup(tokenList[currToken], nouns);
	    if ( result ) {
		System.out.print(tokenList[currToken] + " [noun] ");
		currToken++;
		return true;
	    }
	}
	return false;
    }

    //  <VERB> ::= runs | walks | drives | calls | talks | climbs | falls
    protected static boolean parseVerb() {
	if ( currToken < tokenList.length ) {
	    boolean result = lookup(tokenList[currToken], pluralverbs);
	    if ( result ) {
		System.out.print(tokenList[currToken] + " [verb] ");
		currToken++;
		return true;
	    }
	}
	return false;
    }

    //  <ADV>  ::= quickly | slowly | boldly | fearfully | tearfully | rapidly
    protected static boolean parseAdverb() {
	if ( currToken < tokenList.length ) {
	    boolean result = lookup(tokenList[currToken], adverbs);
	    if ( result ) {
                //  System.out.print(tokenList[currToken] + " [adverb] ");
		currToken++;
		return true;
	    }
	}
	return false;
    }
    //  <PREP> ::= to | from | by | up | after | before | beside | into
    protected static boolean parsePreposition() {
	if ( currToken < tokenList.length ) {
	    boolean result = lookup(tokenList[currToken], prepositions);
	    if ( result ) {
                // System.out.print(tokenList[currToken] + " [preposition] ");
		currToken++;
		return true;
	    }
	}
	return false;
    }
    //  <ADJ>  ::= big | small | short | tall | red | blue | strong | weak
    protected static boolean parseAdjective() {
	if ( currToken < tokenList.length ) {
	    boolean result = lookup(tokenList[currToken], adjectives);
	    if ( result ) {
                // System.out.print(tokenList[currToken] + " [adjective] ");
		currToken++;
		return true;
	    }
	}
	return false;
    }
    //  <ART>  ::= the | a 
    protected static boolean parseArticle() {
	if ( currToken < tokenList.length ) {
	    boolean result = lookup(tokenList[currToken], articles);
	    if ( result ) {
                //  System.out.print(tokenList[currToken] + " [article] ");
		currToken++;
		return true;
	    }
	}
	return false;
    }

*/























	public static void main(String[] args) {
		if ( args.length > 0 ) {
			String inString = "";
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
				inString = reader.readLine();
			} catch ( IOException e ) {
				System.out.println("**** I/O error");
				System.exit(1);
			}
			System.out.println(inString);
			StringTokenizer scanner = new StringTokenizer(inString);
			//  How many words are in the sentence
			tokenList = new String[scanner.countTokens()];
			for ( int i = 0;  i < tokenList.length;  i++ ) {
				tokenList[i] = scanner.nextToken();
			}
			currToken = 0;
			if ( parseSentence() ) {
				System.out.println("Valid sentence");
			} else {
				System.out.println("*Not* a valid sentence");
			}
		} else {
			System.out.println(genSentence());
		}
	}

} // end class parse

